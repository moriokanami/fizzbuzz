import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * 
 */

/**
 * @author morioka.nami
 *
 */

public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("seUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}
	
	//ここからテスト
	
	@Test
	public void isInt_09() {
		System.out.println("9");
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}
	
	@Test
	public void isInt_20() {
		System.out.println("20");
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}
	@Test
	public void isInt_45() {
		System.out.println("45");
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}
	@Test
	public void isInt_44() {
		System.out.println("44");
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}
	@Test
	public void isInt_46() {
		System.out.println("46");
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
